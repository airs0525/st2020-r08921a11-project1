
let sorting = (array) => {
    array.sort();
    return array;
}

let compare = (a, b) => {
    a['PM2.5']
    b['PM2.5']
    if(a<b)
        return a['PM2.5']-b['PM2.5'];
    else if(a>b)
        return a['PM2.5']-b['PM2.5'];
    else
        return a['PM2.5']-b['PM2.5'];
}

let average = (nums) => {
    var sum = 0
    var avg = 0
    for (var i = 0; i < nums.length; i++) {
        var sum = sum + nums[i];
    };
    var avg = sum / (nums.length);
    avg = parseFloat(avg.toFixed(2))
	return avg;
}


module.exports = {
    sorting,
    compare,
    average
}